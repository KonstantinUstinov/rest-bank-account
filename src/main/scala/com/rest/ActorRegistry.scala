package com.rest

import akka.actor.{ActorRef, ActorRefFactory, ActorSystem, Props}
import akka.util.Timeout
import com.rest.controler.BankAccountManager
import scala.concurrent.duration._
import scala.concurrent.Await

object ActorRegistry {

  implicit val timeout = Timeout(15.seconds)

  def init(system: ActorSystem): Unit = {
      val ref = classOf[BankAccountManager]
      system.actorOf(Props(ref), ref.getName)
  }


  def getActor(ref: Class[_], context: ActorRefFactory): ActorRef = {
    Await.result(context.actorSelection(s"/user/${ref.getName}").resolveOne(), 3 seconds)
  }

}
