package com.rest

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.{Http, server}
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.{ActorMaterializer, Materializer}
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import java.io.IOException

import com.rest.Service.{AmountInfo, ErrorDetail}

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.math._
import spray.json.DefaultJsonProtocol
import com.rest.controler.BankAccountManager
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}


object Service {
  case class AmountInfo(amount: Int)
  case class ErrorDetail(code: Int, error: String, message: Option[String], info: Option[String])
}

trait Service extends Protocols {

  implicit val system: ActorSystem
  implicit def executor: ExecutionContextExecutor
  implicit val materializer: Materializer

  def config: Config
  val logger: LoggingAdapter

  lazy val bankAccountManager = ActorRegistry.getActor(classOf[BankAccountManager], system)
  implicit val timeout = Timeout(15.seconds)

  val routes =
    get {
      pathPrefix("balance" / IntNumber) { id =>
        onComplete((bankAccountManager ? BankAccountManager.GetBalance(id)).asInstanceOf[Future[AmountInfo]]) { item =>
          futureHandler(item)
        }
      }
    } ~
    post {
      pathPrefix("deposit" / IntNumber) { id =>
        entity(as[AmountInfo]) { item =>
          onComplete((bankAccountManager ? BankAccountManager.SetDeposit(id, item.amount)).asInstanceOf[Future[AmountInfo]]) { result =>
            futureHandler(result)
          }
        }
      }
    } ~
    post{
      pathPrefix("withdrawal" / IntNumber) { id =>
        entity(as[AmountInfo]) { item =>
          onComplete((bankAccountManager ? BankAccountManager.SetWithdrawal(id, item.amount)).asInstanceOf[Future[AmountInfo]]) { result =>
            futureHandler(result)
          }
        }
      }
    }

  val futureHandler: PartialFunction[Try[Any], server.Route] = {
    case Success(response: AmountInfo)        =>
      complete(200, response)

    case Success(response: ErrorDetail)       =>
      complete(response.code, response)

    case Failure(e: Exception)                =>
      complete(500, ErrorDetail(e.hashCode(), e.toString, Some(e.getMessage), Some(e.getLocalizedMessage)))

    case unknown: Any                         =>
      complete(500, unknown.toString)
  }

}
