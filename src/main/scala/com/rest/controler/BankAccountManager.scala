package com.rest.controler

import java.util.Date
import akka.actor.Actor
import akka.event.Logging
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import com.rest.controler.BankAccountManager._
import com.rest.Service.{AmountInfo, ErrorDetail}
import com.rest.model.TransactionType.TransactionType
import com.rest.model.{AccountTransaction, TransactionType, TransactionsService}
import com.rest.utils.DateUtils

object BankAccountManager {
  case class GetBalance(id : Int)
  case class SetDeposit(id : Int, amount : Int)
  case class SetWithdrawal(id: Int, amount : Int)
}

class BankAccountManager extends Actor with TransactionsService {

  private val MAX_DEPOSIT_PER_TRANSACTION = 40000 // $40k
  private val MAX_DEPOSIT_PER_DAY = 150000 // $150k
  private val MAX_DEPOSIT_TRANSACTIONS_PER_DAY = 4

  private val MAX_WITHDRAWAL_PER_TRANSACTION = 20000 // $20k
  private val MAX_WITHDRAWAL_PER_DAY = 50000 // $50k
  private val MAX_WITHDRAWAL_TRANSACTIONS_PER_DAY = 3

  import context.dispatcher
  final implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(context.system))
  val log = Logging(context.system, this)

  override def receive: Receive = {
    case GetBalance(id) =>
      sender ! AmountInfo(getAmount(id))
    case SetDeposit(id, amount) =>
      val transactions = getTransactions(TransactionType.deposit, id)
      val total = transactions.map(_.amount).sum

      log.debug("total = " + total)

      if (total + amount > MAX_DEPOSIT_PER_DAY)
        sender ! ErrorDetail(501, "Deposit for the day should not be more than $150K", None, None)
      else if (amount > MAX_DEPOSIT_PER_TRANSACTION)
        sender ! ErrorDetail(502, "Deposit per transaction should not be more than $40K", None, None)
      else if (transactions.length >= MAX_DEPOSIT_TRANSACTIONS_PER_DAY)
        sender ! ErrorDetail(503, "Maximum transactions for the day Exceeded", None, None)
      else {
        addTransaction(AccountTransaction(id, amount, new Date(), TransactionType.deposit))
        setAmount(id, getAmount(id) + amount)
        sender ! AmountInfo(getAmount(id))
      }
    case SetWithdrawal(id, amount) =>
      log.debug("set Withdrawal")

      val transactions = getTransactions(TransactionType.withdrawal, id)
      val total = transactions.map(_.amount).sum
      val balance = getAmount(id)

      log.debug("balance = " + balance)
      log.debug("total = " + total)
      log.debug("transactions.size " + transactions.length)

      if (total + amount > MAX_WITHDRAWAL_PER_DAY)
        sender ! ErrorDetail(501, "Withdrawal for the day should not be more than $50K", None, None)
      else if (amount > MAX_WITHDRAWAL_PER_TRANSACTION)
        sender ! ErrorDetail(502, "Withdrawal per transaction should not be more than $20K", None, None)
      else if (transactions.length >= MAX_WITHDRAWAL_TRANSACTIONS_PER_DAY)
        sender ! ErrorDetail(503, "Maximum transactions for the day Exceeded", None, None)
      else if (amount > balance)
        sender ! ErrorDetail(504, "You have insufficient funds", None, None)
      else {
        addTransaction(AccountTransaction(id, amount, new Date(), TransactionType.withdrawal))
        setAmount(id, balance - amount)
        sender ! AmountInfo(getAmount(id))
      }
  }

  private def getTransactions(tt: TransactionType, id: Int) : List[AccountTransaction] =
    findByDateBetweenAndType(id, DateUtils.getStartOfDay(new Date()), DateUtils.getEndOfDay(new Date()), tt)
}
