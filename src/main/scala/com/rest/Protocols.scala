package com.rest

import com.rest.Service.{AmountInfo, ErrorDetail}
import spray.json.DefaultJsonProtocol

trait Protocols extends DefaultJsonProtocol {
  implicit val amountInfoFormat = jsonFormat1(AmountInfo.apply)
  implicit val errorDetailFormat = jsonFormat4(ErrorDetail.apply)
}
