package com.rest.model

import java.util.Date
import com.rest.model.TransactionType.TransactionType

trait TransactionsService {

  private val transactions = scala.collection.mutable.HashSet[AccountTransaction]()
  private val amount = scala.collection.mutable.Map[Int, Int]()

  def addTransaction(transaction: AccountTransaction) = {
    transactions.add(transaction)
  }

  def findByDateBetweenAndType(id: Int, startOfDay: Date, endOfDay: Date, ttype: TransactionType) : List[AccountTransaction] = {
    transactions.filter(transaction => id == transaction.id  && transaction.ttype == ttype && transaction.date.after(startOfDay) && transaction.date.before(endOfDay)).toList
  }

  def getAmount(id: Int)  : Int = {
    amount.getOrElse(id, 0)
  }

  def setAmount(id: Int, amount: Int) = {
    this.amount(id) = amount
  }

}

case class AccountTransaction(id: Int, amount: Int, date: Date, ttype: TransactionType)

object TransactionType extends Enumeration {
  type TransactionType = Value
  val deposit, withdrawal = Value
}