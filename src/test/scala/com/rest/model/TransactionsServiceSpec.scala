package com.rest.model

import org.scalatest.{FlatSpecLike, Matchers}
import java.util.{Calendar, Date}

import com.rest.utils.DateUtils

class TransactionsServiceSpec extends TransactionsService with Matchers with FlatSpecLike {

  "TransactionsService" should "save AccountTransaction" in {
    addTransaction(AccountTransaction(1, 1, new Date(), TransactionType.deposit))
    val result = findByDateBetweenAndType(1, DateUtils.getStartOfDay(new Date()), DateUtils.getEndOfDay(new Date()), TransactionType.deposit)

    result.map(t => AccountTransaction(t.id, t.amount, DateUtils.getEndOfDay(t.date), t.ttype)).head shouldBe AccountTransaction(1, 1, DateUtils.getEndOfDay(new Date()), TransactionType.deposit)
  }

  "TransactionsService" should "return AccountTransaction" in {
    addTransaction(AccountTransaction(2, 2, new Date(), TransactionType.withdrawal))
    addTransaction(AccountTransaction(2, 2, changeSecond(new Date()), TransactionType.withdrawal))

    val result = findByDateBetweenAndType(2, DateUtils.getStartOfDay(new Date()), DateUtils.getEndOfDay(new Date()), TransactionType.withdrawal)

    result.length shouldBe 2
    result.map(t => AccountTransaction(t.id, t.amount, DateUtils.getEndOfDay(t.date), t.ttype)).head shouldBe AccountTransaction(2, 2, DateUtils.getEndOfDay(new Date()), TransactionType.withdrawal)
  }


  "TransactionsService" should "return value if not exists" in {
    getAmount(9) shouldBe 0
  }

  "TransactionsService" should "return saved Amount" in {
    setAmount(7, 7)
    getAmount(7) shouldBe 7
  }

  "TransactionsService" should "return update Amount" in {
    setAmount(7, 7)
    getAmount(7) shouldBe 7

    setAmount(7, 8)
    getAmount(7) shouldBe 8

  }

  def changeSecond(date: Date): Date = {
    val calendar = Calendar.getInstance
    calendar.setTime(date)
    calendar.set(Calendar.MINUTE, 0)
    calendar.set(Calendar.SECOND, 0)
    calendar.getTime
  }
}
