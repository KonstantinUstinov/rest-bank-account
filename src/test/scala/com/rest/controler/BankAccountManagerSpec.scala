package com.rest.controler

import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}
import akka.actor.{ActorSystem, Props}
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit, TestProbe}
import com.rest.Service.{AmountInfo, ErrorDetail}
import com.rest.controler.BankAccountManager.{GetBalance, SetDeposit, SetWithdrawal}


class BankAccountManagerSpec(_system: ActorSystem)
  extends TestKit(_system)
    with DefaultTimeout with ImplicitSender
    with Matchers
    with FlatSpecLike
    with BeforeAndAfterAll
{

  def this() = this(ActorSystem("BankAccountManagerSpec"))

  override def afterAll: Unit = {
    shutdown(system)
  }

  val toggle = system.actorOf(Props[BankAccountManager])
  val p = TestProbe()


  "A BankAccountManagerSpec Actor" should "return 0 amount" in {
    p.send(toggle, GetBalance(1))
    p.expectMsg(AmountInfo(0))
  }

  "A BankAccountManagerSpec Actor" should "Deposit success" in {
    p.send(toggle, SetDeposit(1, 10000))
    p.expectMsg(AmountInfo(10000))
  }

  "A BankAccountManagerSpec Actor" should "return MAX_DEPOSIT_TRANSACTIONS_PER_DAY" in {
    val bankActor = system.actorOf(Props[BankAccountManager])
    val probe = TestProbe()

    probe.send(bankActor, SetDeposit(1, 1001))
    probe.expectMsg(AmountInfo(1001))

    probe.send(bankActor, SetDeposit(1, 1002))
    probe.expectMsg(AmountInfo(2003))

    probe.send(bankActor, SetDeposit(1, 1003))
    probe.expectMsg(AmountInfo(3006))

    probe.send(bankActor, SetDeposit(1, 1004))
    probe.expectMsg(AmountInfo(4010))

    probe.send(bankActor, SetDeposit(1, 1005))
    probe.expectMsg(ErrorDetail(503, "Maximum transactions for the day Exceeded", None, None))
  }


  "A BankAccountManagerSpec Actor" should "return MAX_DEPOSIT_PER_TRANSACTION" in {
    val bankActor = system.actorOf(Props[BankAccountManager])
    val probe = TestProbe()

    probe.send(bankActor, SetDeposit(3, 1001))
    probe.expectMsg(AmountInfo(1001))

    probe.send(bankActor, SetDeposit(3, 1002))
    probe.expectMsg(AmountInfo(2003))

    probe.send(bankActor, SetDeposit(3, 1003))
    probe.expectMsg(AmountInfo(3006))

    probe.send(bankActor, SetDeposit(3, 40001))
    probe.expectMsg(ErrorDetail(502, "Deposit per transaction should not be more than $40K", None, None))
  }

  "A BankAccountManagerSpec Actor" should "return9 MAX_DEPOSIT_PER_DAY" in {
    val actor = system.actorOf(Props[BankAccountManager])
    val pr = TestProbe()

    pr.send(actor, SetDeposit(2, 39999))
    pr.expectMsg(AmountInfo(39999))
    pr.send(actor, SetDeposit(2, 40000))
    pr.expectMsg(AmountInfo(79999))

    pr.send(actor, SetDeposit(2, 39998))
    pr.expectMsg(AmountInfo(119997))

    pr.send(actor, SetDeposit(2, 30004))
    pr.expectMsg(ErrorDetail(501, "Deposit for the day should not be more than $150K", None, None))
  }

  "A BankAccountManagerSpec Actor" should "Withdrawal success" in {

    val actor = system.actorOf(Props[BankAccountManager])
    val pr = TestProbe()

    pr.send(actor, SetDeposit(1, 10000))
    pr.expectMsg(AmountInfo(10000))


    pr.send(actor, SetWithdrawal(1, 10000))
    pr.expectMsg(AmountInfo(0))
  }

  "A BankAccountManagerSpec Actor" should "return MAX_WITHDRAWAL_TRANSACTIONS_PER_DAY" in {
    val bankActor = system.actorOf(Props[BankAccountManager])
    val probe = TestProbe()

    probe.send(bankActor, SetDeposit(90, 10000))
    probe.expectMsg(AmountInfo(10000))

    probe.send(bankActor, SetWithdrawal(90, 1000))
    probe.expectMsg(AmountInfo(9000))

    probe.send(bankActor, SetWithdrawal(90, 1001))
    probe.expectMsg(AmountInfo(7999))

    probe.send(bankActor, SetWithdrawal(90, 1002))
    probe.expectMsg(AmountInfo(6997))

    probe.send(bankActor, SetWithdrawal(90, 1005))
    probe.expectMsg(ErrorDetail(503, "Maximum transactions for the day Exceeded", None, None))
  }

  "A BankAccountManagerSpec Actor" should "insufficient funds" in {
    val bankActor = system.actorOf(Props[BankAccountManager])
    val probe = TestProbe()

    probe.send(bankActor, SetWithdrawal(1, 201))
    probe.expectMsg(ErrorDetail(504, "You have insufficient funds", None, None))

  }

  "A BankAccountManagerSpec Actor" should "return MAX_WITHDRAWAL_PER_TRANSACTION" in {
    val bankActor = system.actorOf(Props[BankAccountManager])
    val probe = TestProbe()

    probe.send(bankActor, SetDeposit(1, 10000))
    probe.expectMsg(AmountInfo(10000))

    probe.send(bankActor, SetWithdrawal(1, 20001))
    probe.expectMsg(ErrorDetail(502, "Withdrawal per transaction should not be more than $20K", None, None))
  }

  "A BankAccountManagerSpec Actor" should "return MAX_WITHDRAWAL_PER_DAY" in {
    val actor = system.actorOf(Props[BankAccountManager])
    val pr = TestProbe()

    pr.send(actor, SetDeposit(1, 40000))
    pr.expectMsg(AmountInfo(40000))

    pr.send(actor, SetDeposit(1, 40000))
    pr.expectMsg(AmountInfo(80000))

    pr.send(actor, SetWithdrawal(1, 20000))
    pr.expectMsg(AmountInfo(60000))

    pr.send(actor, SetWithdrawal(1, 30001))
    pr.expectMsg(ErrorDetail(501, "Withdrawal for the day should not be more than $50K", None, None))
  }

}
