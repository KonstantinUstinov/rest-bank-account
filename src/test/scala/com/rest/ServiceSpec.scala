package com.rest

import akka.actor.Props
import akka.event.Logging
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.rest.Service.AmountInfo
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.ContentTypes._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import com.rest.controler.BankAccountManager

class ServiceSpec
  extends FlatSpec
    with Matchers
    with ScalatestRouteTest
    with Service
    with BeforeAndAfterAll {

  override val config = ConfigFactory.load()
  override val logger = Logging(system, getClass)

  override lazy val bankAccountManager = system.actorOf(Props[BankAccountManager])

  override def afterAll: Unit = {
    system.terminate()
  }

  "Service" should "get AmountInfo" in {
    Get(s"/balance/0") ~> routes ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[AmountInfo] shouldBe AmountInfo(0)
    }
  }

  "Service" should "post deposit" in {
    Post(s"/deposit/0",  AmountInfo(111)) ~> routes ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[AmountInfo] shouldBe AmountInfo(111)
    }
  }

  "Service" should "post withdrawal" in {
    Post(s"/withdrawal/0",  AmountInfo(111)) ~> routes ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[AmountInfo] shouldBe AmountInfo(0)
    }
  }
}
